package com.example.exampleliqubase.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmploymentDTO implements Serializable {
    private Long employmentId;

    private int version;

    private LocalDate startDt;

    private LocalDate endDt;

    private Long workTypeId;

    private String organizationName;

    private String organizationAddress;

    private String positionName;

    private Long personId;
}
