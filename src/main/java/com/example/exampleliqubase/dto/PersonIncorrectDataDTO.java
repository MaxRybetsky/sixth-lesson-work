package com.example.exampleliqubase.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PersonIncorrectDataDTO {
    private String info;

    public PersonIncorrectDataDTO(String info) {
        this.info = info;
    }
}
