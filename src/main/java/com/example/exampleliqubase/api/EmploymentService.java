package com.example.exampleliqubase.api;

import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.model.EmploymentEntity;

import java.util.List;
import java.util.Set;

public interface EmploymentService {
    void update(Long personId, Set<EmploymentDTO> data);

    Set<EmploymentDTO> findAllEmploymentsByPersonId(Long personId);
}
