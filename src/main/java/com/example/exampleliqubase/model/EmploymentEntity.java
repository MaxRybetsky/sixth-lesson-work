package com.example.exampleliqubase.model;

import com.example.exampleliqubase.dto.EmploymentDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class EmploymentEntity {
    private Long employmentId;

    private int version;

    private LocalDate startDt;

    private LocalDate endDt;

    private Long workTypeId;

    private String organizationName;

    private String organizationAddress;

    private String positionName;

    private Long personId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmploymentEntity that = (EmploymentEntity) o;
        return version == that.version
                && Objects.equals(employmentId, that.employmentId)
                && Objects.equals(startDt, that.startDt)
                && Objects.equals(endDt, that.endDt)
                && Objects.equals(workTypeId, that.workTypeId)
                && Objects.equals(organizationName, that.organizationName)
                && Objects.equals(organizationAddress, that.organizationAddress)
                && Objects.equals(positionName, that.positionName)
                && Objects.equals(personId, that.personId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(employmentId, version,
                startDt, endDt,
                workTypeId, organizationName,
                organizationAddress, positionName,
                personId);
    }
}
