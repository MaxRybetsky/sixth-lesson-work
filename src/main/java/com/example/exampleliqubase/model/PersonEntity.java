package com.example.exampleliqubase.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PersonEntity {
    private Long personId;

    private String firstName;

    private String lastName;

    private String middleName;

    private LocalDate birthDate;

    private String gender;
}
