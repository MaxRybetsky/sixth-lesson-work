package com.example.exampleliqubase.service;

import com.example.exampleliqubase.api.EmploymentService;
import com.example.exampleliqubase.dao.EmploymentMapper;
import com.example.exampleliqubase.dao.PersonMapper;
import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.model.EmploymentEntity;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class EmploymentServiceImpl implements EmploymentService {
    private final PersonMapper personMapper;
    private final EmploymentMapper employmentMapper;
    private final ModelMapper mapper;

    @Autowired
    public EmploymentServiceImpl(PersonMapper personMapper,
                                 EmploymentMapper employmentMapper,
                                 ModelMapper mapper) {
        this.personMapper = personMapper;
        this.employmentMapper = employmentMapper;
        this.mapper = mapper;
    }

    @Override
    @Transactional
    public void update(Long personId, Set<EmploymentDTO> data) {
        if(personMapper.findById(personId) == null) {
            throw new NoSuchElementException(
                    String.format("There is no Person with id=%d",personId)
            );
        }
        Set<EmploymentEntity> dbEmployments = employmentMapper.findAllEmploymentsByPersonId(personId);
        Set<EmploymentEntity> newEmployments = mapPersonsEmploymentDtoToEntityAndSaveNew(personId, data);
        removeCommonElements(dbEmployments, newEmployments);
        for (EmploymentEntity oldEmployment : dbEmployments) {
            boolean isInNew = false;
            for (EmploymentEntity newEmployment : newEmployments) {
                if (oldEmployment.getEmploymentId()
                        .equals(newEmployment.getEmploymentId())) {
                    isInNew = true;
                    employmentMapper.update(newEmployment);
                    break;
                }
            }
            if (!isInNew) {
                employmentMapper.deleteById(oldEmployment.getEmploymentId());
            }
        }
    }

    @Override
    @Transactional
    public Set<EmploymentDTO> findAllEmploymentsByPersonId(Long personId) {
        return employmentMapper.findAllEmploymentsByPersonId(personId)
                .stream()
                .map(el -> mapper.map(el, EmploymentDTO.class))
                .collect(Collectors.toSet());
    }

    private void removeCommonElements(Set<EmploymentEntity> first,
                                      Set<EmploymentEntity> second) {
        Set<EmploymentEntity> temp = new HashSet<>(first);
        first.removeAll(second);
        second.removeAll(temp);
    }

    private Set<EmploymentEntity> mapPersonsEmploymentDtoToEntityAndSaveNew(Long personId,
                                                                            Set<EmploymentDTO> data) {
        return data.stream()
                .filter(el -> el.getPersonId().equals(personId))
                .map(el -> mapper.map(el, EmploymentEntity.class))
                .filter(
                        el -> {
                            if (el.getEmploymentId() == null) {
                                employmentMapper.save(el);
                                return false;
                            }
                            return true;
                        }
                )
                .collect(Collectors.toSet());
    }
}
