package com.example.exampleliqubase.controller;

import com.example.exampleliqubase.api.EmploymentService;
import com.example.exampleliqubase.dto.EmploymentDTO;
import com.example.exampleliqubase.dto.PersonIncorrectDataDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.NoSuchElementException;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class EmploymentController {

    @Autowired
    private EmploymentService employmentService;

    @PostMapping("/employment/records/update/{personId}")
    public Set<EmploymentDTO> getListAccounts(
            @PathVariable(name = "personId") Long personId,
            @RequestBody Set<EmploymentDTO> employmentDTOs
            ) {
        employmentService.update(personId, employmentDTOs);
        return employmentService.findAllEmploymentsByPersonId(personId);
    }

    @ExceptionHandler
    public ResponseEntity<PersonIncorrectDataDTO> handleIncorrectId(
            NoSuchElementException exception
    ) {
        PersonIncorrectDataDTO data = new PersonIncorrectDataDTO(exception.getMessage());
        return new ResponseEntity<>(data, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<PersonIncorrectDataDTO> handleIncorrectInput(
            MethodArgumentTypeMismatchException exception
    ) {
        PersonIncorrectDataDTO data = new PersonIncorrectDataDTO(exception.getMessage());
        return new ResponseEntity<>(data, HttpStatus.NOT_FOUND);
    }

}
