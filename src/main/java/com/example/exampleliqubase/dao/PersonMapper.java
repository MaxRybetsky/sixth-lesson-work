package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.model.EmploymentEntity;
import com.example.exampleliqubase.model.PersonEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Mapper
@Repository
public interface PersonMapper {
    @SelectKey(resultType = Long.class, keyProperty = "personId", before = true,
            statement = "select nextval('person_seq')")
    @Insert("insert into person (person_id, first_name, last_name, middle_name, birth_date, " +
            "gender) values (#{personId}, #{firstName} ,#{lastName} ,#{middleName}, #{birthDate}," +
            " #{gender})")
    void save(PersonEntity personEntity);

    @Update("update person set first_name=#{firstName}, last_name=#{lastName}, " +
            " middle_name=#{middleName}, birth_date=#{birthDate}," +
            " gender=#{gender}" +
            " where person_id=#{personId}")
    void update(PersonEntity personEntity);

    @Delete("delete from person where person_id=#{Id}")
    void deleteById(Long id);

    @Select("select * from person where person_id=#{personId}")
    PersonEntity findById(Long personId);

    @Select("select * from person")
    Set<EmploymentEntity> findAll();
}
