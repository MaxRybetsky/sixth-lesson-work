package com.example.exampleliqubase.dao;

import com.example.exampleliqubase.model.EmploymentEntity;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Mapper
@Repository
public interface EmploymentMapper {
    @SelectKey(resultType = Long.class, keyProperty = "employmentId", before = true,
            statement = "select nextval('employment_seq')")
    @Insert("insert into employment (employment_id, version, start_dt, end_dt, work_type_id, " +
            "organization_name, organization_address, position_name, person_id) " +
            "values (#{employmentId}, #{version} ,#{startDt} ,#{endDt}, #{workTypeId}," +
            " #{organizationName}, #{organizationAddress}, #{positionName}," +
            " #{personId} )")
    void save(EmploymentEntity employmentEntity);

    @Update("update employment set version=#{version}, start_dt=#{startDt}, " +
            " end_dt=#{endDt}, work_type_id=#{workTypeId}," +
            " organization_name=#{organizationName}, organization_address=#{organizationAddress}," +
            " position_name=#{positionName}, person_id=#{personId} " +
            "where employment_id=#{employmentId}")
    void update(EmploymentEntity entity);

    @Delete("delete from employment where employment_id=#{employmentId}")
    void deleteById(Long employmentId);

    @Select("select * from employment where employment_id=#{employmentId}")
    EmploymentEntity findById(Long employmentId);

    @Select("select * from employment where person_id=#{personId}")
    Set<EmploymentEntity> findAllEmploymentsByPersonId(Long personId);
}
